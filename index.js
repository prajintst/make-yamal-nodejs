const fs = require('fs');
const YAML = require('json-to-pretty-yaml');
const json = require('./input.json');
 
let data = YAML.stringify(convertTofields(json));
data = data.replace(/"/g,'');
fs.writeFile('output.yaml', data,'utf8',(error)=>{
   console.log("done");
});


function convertTofields(data){
    let fields =[];
    for (var key in data)
    {
      let obj ={"name":key};
     if(typeof (data[key]) ==='object')
     {
        let insidefields =convertTofields(data[key]);
        
        if(insidefields.length){
            obj["fields"]=insidefields;
        }       
     
     }
     else{
       if(key =="id"){
          obj["id"]=true;
       }
     }
     fields.push(obj);
    }
    return fields;
}